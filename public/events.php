<?php
require 'components/script/php/Database.php';

$db = new Database();
$lorem = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi asperiores corporis culpa dolore eum excepturi maxime molestiae mollitia odit porro? Asperiores, cumque delectus doloribus dolorum est illo in iste laboriosam laborum magnam minima nemo odit reiciendis velit, voluptates! Commodi dolorum minus quam unde veniam? Accusantium animi aut autem beatae culpa dolorem ex excepturi facilis hic inventore iste maxime modi molestiae nesciunt nostrum, numquam optio recusandae tempora temporibus ullam. A aliquid exercitationem inventore maxime, saepe vitae. Ab architecto beatae dolorem doloribus ea eius error est fuga iste neque nihil odio omnis perferendis quaerat quasi quod repellat repellendus rerum saepe, similique unde.';

$result = $db->query("select * from event");
?>



<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="components/style/main.css">
    <link rel="stylesheet" href="components/style/navt.css">
    <link rel="stylesheet" href="components/style/events.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">

    <link rel="shortcut icon" href="components/img/logo/favicon_logo.png" type="image/x-icon">

    <title>AJWP </title>
</head>
<body>
<header>
    <?php  require "components/nav.php";
    echo $nav?>
</header>
<div class="container-fluid">
    <div class="events">
        <?php foreach ($result as  $event):?>
            <section class="event_card">
                <header>
                    <img src="<?= $event->img?>" alt="">
                </header>
                <footer>
                    <p class="title">
                        <?= $event->title?>
                    </p>
                    <section class="info">
                        <div class="dates">
                                <?php
                                    $dateTime = new DateTime($event->starts_at);
                                    $calendar = $dateTime->format("Y-m-d");
                                    $time = $dateTime->format("H:i")
                                ?>
                                <span>
                                    <i class="far fa-calendar-alt"></i>
                                    <?=$calendar?>
                                </span>
                                <span>
                                    <i class="far fa-clock"></i>
                                    <?=$time?>
                                </span>
                            </div>



                        <div class="location">
                            <p>
                                <i class="fas fa-map-marker-alt"></i>
                                <span>
                                Adresse
                            </span>
                            </p>
                            <p><?= $event->adress?></p>
                            <p><?= $event->city?></p>
                        </div>

                    </section>
                </footer>

                <a href="event.php?event=<?=$event->id?>">
                    <div class="article-link">
                        <span class="button_bg"></span>
                        <span>Visiter</span>

                    </div>
                </a>

            </section>


        <?php endforeach;?>

    </div>

</div>


</body>

<script src="components/script/js/nav.js"></script>
</html>