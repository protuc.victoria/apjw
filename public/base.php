<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="components/style/main.css">
    <link rel="stylesheet" href="components/style/navt.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">

    <link rel="shortcut icon" href="components/img/logo/favicon_logo.png" type="image/x-icon">

    <title>AJWP </title>
</head>
<body>
<header>
    <?php  require "components/nav.php";
    echo $nav?>
</header>
<div class="container-fluid">


</div>


</body>

<script src="components/script/js/nav.js"></script>
</html>