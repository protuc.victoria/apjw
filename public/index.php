<?php


?>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="components/style/main.css">
    <link rel="stylesheet" href="components/style/navt.css">
    <link rel="stylesheet" href="components/style/style.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">

    <link rel="shortcut icon" href="components/img/logo/favicon_logo.png" type="image/x-icon">

    <title>APJW - Home</title>
</head>
<body>
<header>
    <?php  require "components/nav.php";
    echo $nav?>
</header>
<div class="container-fluid">

    <div class="parallax">
        <div class="parallaxText" >
            <span class="parallaxBorder">
                <span class="textSpace">APJW - gaming association</span>
            </span>
        </div>
    </div>


    <section class="about_organisation"></section>


    <section class="about_creators" >

        <h3><span>Fondateurs APJW</span></h3>
        <div class="cards">
            <div class="card">
                <header>
                    <div class="avatar">
                        <img src="components/otherSources/G4m3ur.jpg" alt="">
                    </div>
                </header>

                <footer>
                    <span>
                         Président de l’association.
                    </span>
                    <p>
                        La manette est presque devenue une extension de son corps. Son habilité
                        aux FPS frise le super pouvoir. Il a été dit qu’on l’aurait aperçu une fois en dehors de sa cave.
                    </p>
                </footer>
            </div>
            <div class="card">
                <header>
                    <div class="avatar">
                        <img src="components/otherSources/Gandoulf.jpg" alt="">
                    </div>
                </header>

                <footer>
                    <span>
                         Simple mortel de l’association.
                    </span>
                    <p>
                        Il essaye de faire croire que sa barbe est vraie, même si personne
                        n’est dupe. Fan de jeux de rôle sur plateau comme à l’écran, il connaît tous les sorts de Donjons et
                        Dragons par coeur !

                    </p>
                </footer>
            </div>
            <div class="card">
                <header>
                    <div class="avatar">
                        <img src="components/otherSources/Steampunkette.jpg" alt="">
                    </div>
                </header>

                <footer>
                    <span class="hover">
                         Trésorière de l’association.
                    </span>
                    <p>
                        Fan de l’univers steampunk, de cyberpunk et de cosplay. Pour elle
                        l’important c’est le fun. Elle souhaite que le monde vidéoludique soit plus ouvert et plus accessible
                        à tous.

                    </p>
                </footer>
            </div>

        </div>


    </section>

    <section id="imgSection">

    </section>



</div>


</body>


<script src="components/script/js/nav.js"></script>
</html>
