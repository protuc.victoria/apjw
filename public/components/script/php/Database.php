<?php


class Database
{
    public $link;
    private $db_config = array(
        'db_name' => 'apjw',
        'db_user' => 'apjw',
        'db_pass' => 'apjw',
        'db_host'=> 'vhodoroh.com',
        'charset' => 'utf8'
    );

    public function __construct()
    {

    }
    private function pre_connect()
    {
        foreach ($this->db_config as $key => $value) {
            if ($value === null) {
                return false;
            }
        }
        return true;
    }

    private function connect()
    {
        if ($this->link === null) {
            if (!$this->pre_connect())
                die;
            else {
                $dsn = 'mysql:host='.$this->db_config['db_host'].';dbname='.$this->db_config['db_name'].';charset='.$this->db_config['charset'];
                $this->link = new PDO($dsn,$this->db_config['db_user'],$this->db_config['db_pass']);
                $this->link->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            }
        }
        return $this->link;
    }
    public function execute($sql)
    {
        $sth = $this->connect()->prepare($sql);
        return $sth->execute();
    }
    public function query($statement )
    {
        $req = $this->connect()->query($statement);
        $req->setFetchMode(PDO::FETCH_OBJ);
        $datas = $req->fetchAll();

        return $datas;
    }

//    public function prepare($statement, $attributes)
//    {
//        $req = $this->connect()->prepare($statement);
//        $req->execute($attributes);
//        $req->setFetchMode(PDO::FETCH_OBJ);
//        $datas = $req->fetchAll();
//
//        return $datas;
//    }
    public function prepare($statement, $attributes)
    {
        $req = $this->connect()->prepare($statement)->execute($attributes);
    }


    /**
     * @param $table
     * @param array $cols
     * @param array $data
     * @param $where
     * @param $place
     * @return bool
     */
    public function update($table, $cols = [], $data = [], $where, $place){
        $query = "";
        if (!empty($data) && !empty($cols)){
            if (count($cols) === count($data)){
                $query = "update {$table} set ";
                for ($i = 0, $iMax = count($data); $i < $iMax; $i++){
                    $query .= " $cols[$i]={$data[$i]},";
                }
                $query = substr($query,0,-1);
                $query .= " where {$where}={$place}";
            }
        }
        if ($query !== "")
            return $this->execute($query);
    }
    public function insert($table,$data = []){
        $query= 'INSERT INTO '.$table.' (';


        foreach ($data as $key => $value){
            $query .= $key.',';
        }
        $query = substr($query,0,-1).') VALUES(';
        foreach ($data as $key => $value){
            $query .= $value.',';
        }
        $query = substr($query,0,-1).')';
//            var_dump($query);
        return $this->execute($query);
    }

}