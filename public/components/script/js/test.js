
console.log([1,2,3].map(function (x) {
   const y = x +1;
   return x*y;
}));

console.log([1,2,3].map((x) => {
   const y = x+1;
   return x * y;
}));

console.log([1, 4, 8].map((number) => `A string containing the ${number + 1}.`));
