<?php
require 'components/script/php/Database.php';

$db = new Database();
$event = null;

if (isset($_GET['event'])) $event = $_GET['event'];
else header('HTTP/1.0 404 Not Found');
$cookie = array();

    if(isset($_COOKIE['author']) && isset($_COOKIE['message'])){
        $cookie['author'] = $_COOKIE['author'];
        $cookie['message'] = $_COOKIE['message'];

    }
$result = current($db->query("select * from event where id=${event}"));
try{

    $coments = ($db->query(
        "select  author, text, created_at
                    from comment
                    where event_id = {$event}
                     "));
}catch (Exception $event){

}



?>


<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="components/style/main.css">
    <link rel="stylesheet" href="components/style/navt.css">
    <link rel="stylesheet" href="components/style/event.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
    <link rel="shortcut icon" href="components/img/logo/favicon_logo.png" type="image/x-icon">

    <title>AJWP </title>
</head>
<body>


<header>
    <?php  require "components/nav.php";
    echo $nav?>
</header>
<div class="container">

    <article>
        <header>
            <div class="parallax" >
                <style>.parallax{background: url("<?=$result->img?>") no-repeat fixed center;}</style>

                <div class="parallaxText" id="ptext">
            <span class="parallaxBorder">
                <span class="textSpace">Evenement</span>
            </span>
                </div>
            </div>
        </header>
        <div class="content">

            <section class="description">
                <h3><?=$result->title?></h3>
                <p>
                    <?=$result->description?>
                </p>

            </section>
            <footer>
                <?php
                $dateTime = new DateTime($event->starts_at);
                $calendar = $dateTime->format("Y-m-d");
                $time = $dateTime->format("H:i")
                ?>
                <span>

                </span>
                <span>
                    <i class="far fa-calendar-alt"></i>
                    <?=$calendar?>
                </span>
                <span>
                    <i class="far fa-clock"></i>
                    <?=$time?>
                </span>
            </footer>
        </div>

    </article>

    <section class="comments">

        <div class="comment_form">
            <h3>Laisser un commentaire</h3>

            <form method="POST" id="comment_form" action="components/script/php/comment_insert.php">
                <div>
                    <input id="name" name="name" type="text" required value="<?=$cookie['author']??=''?>">
                    <label for="name">Nom</label>
                </div>

                <div>
                    <textarea id="message" name="comment" required><?=$cookie['message']??=''?></textarea>
                    <label for="message">Message</label>
                </div>
                <div>
                    <input type="text" hidden value="<?= $event?>">
                </div>
                <button class="submit" type="submit" aria-describedby="Envoyer">
                    Envoyer
                    <i class="material-icons">
                        send
                    </i></button>
            </form>
        </div>
        <?php foreach ($coments as $comment):?>
            <div class="comment_block">
                <span class="name"><?=$comment->name." ".$comment->last_name?></span>
                <span class="date"><?=$comment->created_at?></span>
                <p>
                    <?=$comment->text?>
                </p>
            </div>

        <?php endforeach;?>

    </section>
</div>


</body>


<script src="components/script/js/nav.js"></script>
<script src="components/script/js/form.js"></script>

